<?php
session_start();
session_destroy();

header("Location: https://{$_SERVER['SERVER_NAME']}/dashboard/login/");
?>