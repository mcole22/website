<?php
class User extends DB {
    private $uid;
    private $name = [];
    private $username;
    private $permissions;
    private $loggedin;

    public function __construct($ignoreValidation = false) {
        parent::__construct(['db_name' => 'ammmzmdv_website_qa']);

        $this->uid = (!isset($_SESSION['id'])) ?: $_SESSION['id'];
        $this->name[] = (!isset($_SESSION['name'][0])) ?: $_SESSION['name'][0];
        $this->name[] = (!isset($_SESSION['name'][1])) ?: $_SESSION['name'][1];
        $this->username = (!isset($_SESSION['username'])) ?: $_SESSION['username'];
        $this->permissions = (!isset($_SESSION['permissions'])) ? [] : $_SESSION['permissions'];
        $this->loggedin = (isset($_SESSION['loggedin']));

        if(!$ignoreValidation) {
            $this->getLoginStatus();
            $v = ($this->getField('valid_to', 'uid', $this->uid)[0]['valid_to'] !== date("Y-m-d", strtotime('-1 day'))) ?: exit($this->query("UPDATE users SET approved = 0 WHERE uid = {$this->uid}"));
            $v = ($this->getField('approved', 'uid', $this->uid)[0]['approved'] == 1) ?: exit('Your account is awaiting approval. Please contact the system administrator for more information.');
        }
    }

    public function getAccess($pid, $type = 'p') {
        if(is_array($pid)) {
            $r = true;
            foreach($pid as $p) {
                if(!in_array($p, $this->permissions))
                    $r = false;
            }

            return $r;
        } else {
            return ($type == 'd') ?: in_array($pid, $this->permissions);
        }
    }

    public function getLoginStatus() {
        if(!$this->loggedin) {
            header("Location: https://{$_SERVER['SERVER_NAME']}/dashboard/login/");
            exit;
        }
    }

    public function getField($k, $c = null, $v = null) {
        $r = (!$c) ? $this->select("SELECT {$k} FROM users") : $this->select("SELECT {$k} FROM users WHERE {$c} = {$v}");
        return $r;
    }

    public function setField($kv, $uid) {
        $stmt = "UPDATE users SET ";
        foreach($kv as $k => $v) {
            $stmt .= "{$k} = '{$v}'";
            if($k !== array_key_last($kv)) { $stmt .= ', '; }
        }
        $stmt .= " WHERE uid = {$uid}";
        $this->query($stmt);
    }

    public function getRoles() {
        return $this->select("SELECT * FROM roles");
    }

    public function getUser($kv, $c, $v, $l = null) {
        $i = 1;
        $stmt = "SELECT ";
        foreach($kv as $k) {
            $stmt .= "{$k}";
            if($i !== count($kv)) { $stmt .= ', '; }

            $i++;
        }

        $stmt .= " FROM users WHERE {$c} = {$v}";
        if($l)
            $stmt .= " LIMIT {$l}";
            
        return $this->select($stmt);
    }

    public function rejectUser($uid) {
        return $this->query("DELETE FROM users WHERE uid = {$uid}");
    }

    public function checkAccess($pid, $type = 'p') {
        if(!$this->getAccess($pid, $type))
            header("Location: https://{$_SERVER['SERVER_NAME']}/dashboard/");
    }
}