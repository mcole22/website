<?php
class Login extends DB {
    public function authenticate($username, $password) {
        if(!isset($username, $password)) {
            exit('Please fill in both fields!');
        }

        $r = $this->select("SELECT uid, password, firstname, lastname, rid FROM users WHERE username = :username", [':username' => $_POST['username']]);

        if(empty($r))
            exit('Incorrect username and/or password!');

        if(password_verify($_POST['password'], $r[0]['password'])) {
            session_regenerate_id();
            
            $_SESSION['loggedin'] = true;
            $_SESSION['name'][0] = $r[0]['firstname'];
            $_SESSION['name'][1] = $r[0]['lastname'];
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['id'] = $r[0]['uid'];

            $p = $this->select("SELECT pid FROM roles_permissions WHERE rid = :rid", [':rid' => $r[0]['rid']]);
            foreach($p as $k => $v) {
                $_SESSION['permissions'][] = $v['pid'];
            }
            
            header("Location: https://{$_SERVER['SERVER_NAME']}/dashboard/");
        } else {
            echo 'Incorrect username and/or password!';
        }
    }

    public function register($username, $password, $email) {
        $this->validate_input_register($username, $password, $email);

        $r = $this->query("SELECT uid, password FROM users WHERE username = :username", [':username' => $_POST['username']]);
        if($r > 0) { 
            echo 'Username exists!'; 
        } else {
            $creationDate = date("Y-m-d");
            $validFromDate = date("Y-m-d", strtotime('-1 day'));
            $validToDate = date("Y-m-d", strtotime('+1 year'));
            $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

            $this->query("INSERT INTO users (created_at, username, password, email, rid, approved, valid_from, valid_to) VALUES (:created_at, :username, :password, :email, 1, 0, :valid_from, :valid_to)", [':created_at' => $creationDate, ':username' => $_POST['username'], ':password' => $password, ':email' => $_POST['email'], ':valid_from' => $validFromDate, ':valid_to' => $validToDate]);
            
            echo 'You have successfully registered, you can now login!';
        }
    }

    private function validate_input_register($username, $password, $email) {
        if(!isset($username, $password, $email)) {
            exit('Please complete the registration form!');
        }
    
        if(empty($username) || empty($password) || empty($email)) {
            exit('Please complete the registration form');
        }
    
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            exit('Email is not valid!');
        }
        
        if (preg_match('/^[a-zA-Z0-9.]+$/', $username) == 0) {
            exit('Username is not valid!');
        }
        
        if (strlen($password) > 20 || strlen($password) < 5) {
            exit('Password must be between 5 and 20 characters long!');
        }
    }
}