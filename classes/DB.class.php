<?php
class DB {
    private $settings = [
        'hostname' => '127.0.0.1',
        'user' => 'ammmzmdv_qauser',
        'password' => 'q$MxLVoA?#?X',
        'db_name' => '',
        'port' => '3306',
        'chartset' => 'utf8mb4'
    ];
    
    private $connection;

    public function __construct($settings, $passFile = false){
        $this->settings = array_replace($this->settings, $settings);

        if ($passFile){
            $this->settings['password'] = trim(file_get_contents($passFile));
        }

        $this->connection = new PDO($this->getConnectionString(), $this->settings['user'], $this->settings['password']);
    }

    protected function getConnectionString(){
        return "mysql:dbname={$this->settings['db_name']};host={$this->settings['hostname']};port={$this->settings['port']};charset=utf8mb4";
    }

    protected function prep($q){return $this->connection->prepare($q);}

    protected function fill($stmt, $args = array()){
        foreach ($args as $name => $arg){
            $type = PDO::PARAM_STR;
            if (is_numeric($arg)){
                $type = PDO::PARAM_INT;
            } elseif (is_bool($arg)) {
                $type = PDO::PARAM_BOOL;
            }
            $stmt->bindValue($name, $arg, $type);
        }
        return $stmt;
    }

    protected function make($q, $a = array()){
        $stmt = $this->prep($q);
        return $this->fill($stmt, $a);
    }

    protected function query($q, $a = array()){
        $stmt = $this->make($q, $a);
        $succ = $stmt->execute();
        if (!$succ){throw new PDOException($stmt->errorInfo()[2], $stmt->errorInfo()[1]);}
        return $stmt->rowCount();
    }

    protected function select($q, $a = array(), $mode = PDO::FETCH_ASSOC){
        $stmt = $this->make($q, $a);
        $succ = $stmt->execute();
        if (!$succ){throw new PDOException($stmt->errorInfo()[2], $stmt->errorInfo()[1]);}
        return $stmt->fetchAll($mode);
    }

    protected function return_stmt($q, $a = array()){
        $stmt = $this->make($q, $a);
        $succ = $stmt->execute();
        if (!$succ){throw new PDOException($stmt->errorInfo()[2], $stmt->errorInfo()[1]);}
        return $stmt;
    }
}