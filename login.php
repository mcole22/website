<!DOCTYPE html>
<html>
    <head>
        <?php require_once('./includes/templates/header.tmp.php'); ?>
    </head>
    <body>   
        <div class="login">
            <h1>AMM Motorsport</h1>
            <ul class="nav nav-pills justify-content-center">
                <li class="nav-item">
                    <a class="nav-link active" id="loginPill" data-toggle="pill" href="#login" onclick="$('a#registerPill, #register').removeClass('active'); $(this).addClass('active');">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="registerPill" data-toggle="pill" href="#register" onclick="$('a#loginPill, #login').removeClass('active'); $(this).addClass('active');">Register</a>
                </li>
            </ul>

            <div class="tab-content">
                <div id="login" class="tab-pane active">
                    <form action="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/includes/authenticate.php" method="post">
                        <label for="username">
                            <i class="fas fa-user"></i>
                        </label>
                        <input type="text" name="username" placeholder="Username" id="username" required>

                        <label for="password">
                            <i class="fas fa-lock"></i>
                        </label>
                        <input type="password" name="password" placeholder="Password" id="password" required>

                        <input type="submit" value="Submit">
                    </form>
                </div>

                <div id="register" class="tab-pane">
                    <form action="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/includes/register.php" method="post" autocomplete="off">
                        <label for="username">
                            <i class="fas fa-user"></i>
                        </label>
                        <input type="text" name="username" placeholder="Username" id="username" required>
                        <label for="password">
                            <i class="fas fa-lock"></i>
                        </label>
                        <input type="password" name="password" placeholder="Password" id="password" required>
                        <label for="email">
                            <i class="fas fa-envelope"></i>
                        </label>
                        <input type="email" name="email" placeholder="Email" id="email" required>
                        <input type="submit" value="Submit">
                    </form>
                </div>
            </div>
        </div>
    </body>

    <?php require_once('./includes/templates/footer.tmp.php'); ?>
</html>