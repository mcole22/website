<?php
session_start();

require_once('./includes/autoloader.php');

$user = new User();
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require_once('./includes/templates/header.tmp.php'); ?>
    </head>
    <body class="loggedin">
        <?php require_once('./includes/templates/nav.tmp.php'); ?>

        <div class="container--dashboard">
            <h1>Home Page</h1>
        </div>

        <div class="toasts--dashboard">
            <div class="toast-container">
                <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <i class="fa-solid fa-triangle-exclamation me-2"></i>
                        <strong class="me-auto">Security Notification</strong>
                        <small class="text-light">just now</small>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                        Your account is due to expire in 7 days! Please contact an administrator.
                    </div>
                </div>

                <div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <i class="fa-solid fa-triangle-exclamation me-2"></i>
                        <strong class="me-auto">Edited User</strong>
                        <small class="text-light">2 seconds ago</small>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                        Your changes to user 'm.cole' have been successfully saved!
                    </div>
                </div>
            </div>
        </div>
    </body>

    <?php require_once('./includes/templates/footer.tmp.php'); ?>
</html>