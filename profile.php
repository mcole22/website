<?php
session_start();

require_once('./includes/autoloader.php');

$user = new User();

$r = $user->getField('email', 'uid', $_SESSION['id']);
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require_once('./includes/templates/header.tmp.php'); ?>
    </head>
    <body class="loggedin">
        <?php require_once('./includes/templates/nav.tmp.php'); ?>

        <div class="content">
			<h2>Profile Page</h2>
			<div>
				<p>Your account details are below:</p>
				<table>
					<tr>
						<td>Username:</td>
						<td><?=$_SESSION['username']?></td>
					</tr>
					<tr>
						<td>Full Name:</td>
						<td><?=$_SESSION['name'][0]?> <?=$_SESSION['name'][1]?></td>
					</tr>
					<tr>
						<td>Email Address:</td>
						<td><?=$r[0]['email']?></td>
					</tr>
				</table>
			</div>
		</div>
    </body>

    <?php require_once('./includes/templates/footer.tmp.php'); ?>
</html>