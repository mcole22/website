<?php
session_start();

require_once('../includes/autoloader.php');

$user = new User();
$c = $user->checkAccess(5);
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require_once('../includes/templates/header.tmp.php'); ?>
    </head>
    <body class="loggedin">
        <?php require_once('../includes/templates/nav.tmp.php'); ?>

        <div class="container--dashboard">
            <h1>Admin CP</h1>

            <div class="container-fluid container__db">
                <div class="row">
                    <div class="col-lg-9 d-flex justify-content-center">
                        <div class="link-square link-square__db">
                            <a class="link" href="./users/">
                                <div class="link-content">
                                    <i class="fa-solid fa-users-gear"></i><br>
                                    User Mgmt
                                </div>
                            </a>
                        </div>
                        <div class="link-square link-square__db">
                            <a class="link" href="./roles/">
                                <div class="link-content">
                                    <i class="fa-solid fa-hammer"></i><br>
                                    Role Mgmt
                                </div>
                            </a>
                        </div>
                        <div class="link-square link-square__db">
                            <a class="link" href="./logs/">
                                <div class="link-content">
                                <i class="fa-solid fa-file-lines"></i><br>
                                    Logs
                                </div>
                            </a>
                        </div>
                        <div class="link-square link-square__db">
                            <a class="link" href="./settings/">
                                <div class="link-content">
                                    <i class="fa-solid fa-gear"></i><br>
                                    Settings
                                </div>
                            </a>
                        </div>
                        <div class="link-square link-square__db">
                            <a class="link" href="https://www.amm-motorsport.online/">
                                <div class="link-content">
                                    <i class="fa-solid fa-arrow-up-right-from-square"></i><br>
                                    Live Server
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 justify-content-center">
                        <div class="infobar infobar__db">
                            <h6>Users Awaiting Approval</h6>
                            <table class="table table__db">
                                <tbody>
                                    <?php 
                                        $approved = ['No', 'Yes'];
                                        $getUsers = $user->getUser(['uid', 'created_at', 'username', 'firstname', 'lastname'], 'approved', 0, 5);
                                        foreach($getUsers as $u) :
                                            $u['firstname'] = (!empty($u['firstname'])) ? $u['firstname'] : 'Name not set.'; 
                                    ?>
                                        <tr>
                                            <td><?=$u['created_at']?></td>
                                            <td><?=$u['username']?><br><em><?=$u['firstname']?> <?=$u['lastname']?></em></td>
                                            <td align="right"><a href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/admin/editUser?uid=<?=$u['uid']?>&username=<?=$u['username']?>" target="_blank"><i class="fa-solid fa-eye"></i></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <h6>Useful Links</h6>
                    <p>League Spreadsheet - https://docs.google.com/spreadsheets/d/1MgsdZJs4yXLQXpVsk4EDy-QgA_1gopoLA7gadQRFOjY/edit#gid=741297496</p>
                </div>
            </div>

            
        </div>

        <div class="toasts--dashboard">
            <div class="toast-container">
                <div class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <i class="fa-solid fa-triangle-exclamation me-2"></i>
                        <strong class="me-auto">Security Notification</strong>
                        <small class="text-light">just now</small>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                        Your account is due to expire in 7 days! Please contact an administrator.
                    </div>
                </div>

                <div class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">
                        <i class="fa-solid fa-triangle-exclamation me-2"></i>
                        <strong class="me-auto">Edited User</strong>
                        <small class="text-light">2 seconds ago</small>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                    <div class="toast-body">
                        Your changes to user 'm.cole' have been successfully saved!
                    </div>
                </div>
            </div>
        </div>
    </body>

    <?php require_once('../includes/templates/footer.tmp.php'); ?>
</html>