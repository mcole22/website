<?php
session_start();

require_once('../includes/autoloader.php');

$user = new User();
$c = $user->checkAccess(5);
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require_once('../includes/templates/header.tmp.php'); ?>
    </head>
    <body class="loggedin">
        <?php require_once('../includes/templates/nav.tmp.php'); ?>

        <div class="container--dashboard">
            <h1>Security Logs</h1>

            <div class="container-fluid container__db">
                <div class="row justify-content-center">
                
                </div>
            </div>
        </div>
    </body>

    <?php require_once('../includes/templates/footer.tmp.php'); ?>
</html>