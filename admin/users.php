<?php
session_start();

require_once('../includes/autoloader.php');

$user = new User();
$c = $user->checkAccess(5);
?>


<!DOCTYPE html>
<html>
    <head>
        <?php require_once('../includes/templates/header.tmp.php'); ?>
    </head>
    <body class="loggedin">
        <?php require_once('../includes/templates/nav.tmp.php'); ?>

        <div class="content">
            <h2>Users Page</h2>
            <div class="container">
                <input type="text" class="form-control" id="search" placeholder="Search for user...">
                <table class="table table-hover table-sm align-middle text-center">
                    <thead>
                        <tr>
                            <th scope="col">UID</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">Name</th>
                            <th scope="col">Approved</th>
                        </tr>
                    </thead>
                    <tbody id="users">
                        <?php 
                            $approved = ['No', 'Yes'];
                            $allUsers = $user->getField('uid, username, email, firstname, lastname, approved');
                            foreach($allUsers as $u) : 
                        ?>
                            <tr>
                                <th scope='row'><?=$u['uid']?></th>
                                <td><a href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/admin/editUser?uid=<?=$u['uid']?>&username=<?=$u['username']?>" target="_blank" class="link-dark"><?=$u['username']?></a></td>
                                <td><?=$u['email']?></td>
                                <td><?=$u['firstname']?> <?=$u['lastname']?></td>
                                <td><?=$approved[$u['approved']]?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>

    <?php require_once('../includes/templates/footer.tmp.php'); ?>
    <script>
        $(document).ready(function(){
            $("#search").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#users tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>
</html>