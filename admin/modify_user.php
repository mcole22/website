<?php
session_start();

require_once('../includes/autoloader.php');

$user = new User();
$v = $user->getAccess(5) ?: header("Location: http://localhost/amm/f1/");

if(!isset($_GET['uid']) || !isset($_GET['username'])) {
    exit('Invalid user!');
}

$u = $user->getField('uid, created_at, username, email, rid, approved, valid_from, valid_to', 'uid', $_GET['uid']);
$u = (!empty($u)) ? $u : exit('Invalid user!');

$r = $user->getRoles();
?>

<!DOCTYPE html>
<html>
    <head>
        <?php require_once('../includes/templates/header.tmp.php'); ?>
    </head>
    <body class="loggedin">
        <div class="content modify-user" style="margin-top:100px">
        <h2>User Info</h2>
            <div class="container d-flex justify-content-center">
                <form method="post" action="../includes/forms/modify_user.php">
                    <input type="hidden" name="uid" value="<?=$u[0]['uid']?>">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-floating">
                                <input type="text" class="form-control" id="username" name="username" value="<?=$u[0]['username']?>" disabled>
                                <label for="username">User ID</label>    
                            </div>
                        </div>   
                    </div>
                    <div class="row mb-3 text-center">
                        <label class="col-md-4 pt-1">Approval:</label>
                        <div class="col-md-8">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="approved" id="approvedY" value="1" <?php if($u[0]['approved'] == 1) echo "checked"; ?>>
                                <label class="form-check-label radio" for="approvedY">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="approved" id="approvedN" value="0" <?php if($u[0]['approved'] == 0) echo "checked"; ?>>
                                <label class="form-check-label radio" for="approvedN">No</label>
                            </div>
                            <button type="submit" class="btn btn-danger" id='reject' formaction="../includes/forms/reject_user.php">Reject User</button>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-floating">
                                <input type="email" class="form-control" id="email" name="email" value="<?=$u[0]['email']?>" required>
                                <label for="email">Email</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-floating">
                                <select name="rid" id="rid" class="form-select">
                                    <?php foreach($r as $k => $v) : ?>
                                    <option value="<?=$v['uid']?>" <?php if($v['uid'] == $u[0]['rid']) echo 'selected'; ?>><?=$v['name']?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="rid">Role</label>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="date" class="form-control" id="valid_from" name="valid_from" value="<?=$u[0]['valid_from']?>">
                                <label for="valid_from">Valid from</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-floating">
                                <input type="date" class="form-control" id="valid_to" name="valid_to" value="<?=$u[0]['valid_to']?>">
                                <label for="valid_to">Valid to</label>
                            </div>
                        </div>
                    </div>
                    <div class="gap-2 d-flex justify-content-end mb-3">
                        <button type="submit" class="btn btn-primary text-right" id="save">Save</button>
                        <button type="button" class="btn btn-secondary" id="cancel">Cancel</button>
                        <button type="button" class="btn btn-outline-warning" id="edit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </body>

    <?php require_once('../includes/templates/footer.tmp.php'); ?>
    <script type='text/javascript'>
        $(document).ready(function() {
            let state = false;
            let formInputs = ['button#save', 'button#reject', 'input#approvedY', 'input#approvedN', 'input#email', 'select#rid', 'input#valid_from', 'input#valid_to'];

            function toggleInputState() {
                state = !state;
                
                formInputs.forEach(i => {
                    $(i).attr('disabled', state);
                });
            }

            function cancelEdit() {
                if(!state) { 
                    toggleInputState(); 
                } else {
                    window.close();
                }
            }

            toggleInputState();

            $('button#edit').click(function() {
                toggleInputState();
                $(this).attr('disabled', true);
            });

            $('button#cancel').click(function() {
                cancelEdit();
                $('button#edit').attr('disabled', false);
            });
        });
    </script>
</html>