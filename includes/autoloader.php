<?php
function classAutoloader($className) {
    $path = dirname(__FILE__, 2) . "/classes/";
    $extension = ".class.php";
    $fullPath = $path . $className . $extension;

    include_once $fullPath;
}

spl_autoload_register('classAutoloader');