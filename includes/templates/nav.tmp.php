<nav class="navbar navbar-expand-lg navbar-dark">
	<div class="container-fluid">
		<!-- <a href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/" class="navbar-brand">QA SERVER | AMM Motorsport</a> -->
		<a href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/" class="navbar-brand d-flex"><img src="https://db-qa.amm-motorsport.online/dashboard/assets/img/logo2.png" alt="logo2" class="my-auto" style="height:33px;"> <em>QA</em></h1></a>
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#nav" aria-controls="nav" aria-expanded="false" aria-label="Toggle navigation">
      		<span class="navbar-toggler-icon"></span>
    	</button>
		<div class="collapse navbar-collapse" id="nav">
			<ul class="navbar-nav ms-auto mb-2 mb-lg-0 justify-content-end">

				
				<li class="nav-item dropdown">
					<a href="#" class="nav-link dropdown-toggle" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"><img src="https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png" alt="Avatar" height="30" width="30" class="rounded-circle text-align-top"></a>
					<ul class="dropdown-menu dropdown-menu-end dropdown-menu-dark" aria-labelledby="userDropdown">
						<li><a href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/profile/" class="dropdown-item"><i class="fas fa-user-circle"></i> Profile</a></li>
						<li><a href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/logout/" class="dropdown-item"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
					</ul>
				</li>				
			</ul>
		</div>
	</div>
</nav>

<nav class="col-md-2 d-none d-md-block sidebar">
	<div class="sidebar-sticky d-flex">
		<ul class="nav flex-column">		           
			<?php if($user->getAccess(5)) : ?>
	    		<li class="nav-item"><a class="nav-link" href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/admin/"><i class="fa-solid fa-user-shield fa-xl"></i></a></li>
	    		<li class="nav-item"><a class="nav-link" href="https://www.amm-motorsport.online/"><i class="fa-solid fa-arrow-up-right-from-square fa-xl"></i></a></li>
			<?php endif; ?>

	        <li class="nav-item"><a class="nav-link" href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/results/"><i class="fa-solid fa-list-ol fa-xl"></i></a></li>
	        <li class="nav-item"><a class="nav-link" href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/polls/"><i class="fa-solid fa-square-poll-horizontal fa-xl"></i></a></li>
	        <li class="nav-item"><a class="nav-link" href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/gallery/"><i class="fa-solid fa-images fa-xl"></i></a></li>
	        <li class="nav-item"><a class="nav-link" href="https://<?=$_SERVER['SERVER_NAME']?>/dashboard/faq/"><i class="far fa-question-circle fa-xl"></i></a></li>
	    </ul>
	</div>
</nav>