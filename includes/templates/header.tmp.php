<?php
$titles = [
    '' => 'Index',
    'index' => 'Index',
    'profile' => 'Profile Page',
    'login' => 'Welcome',
    'register' => 'Welcome',
    'users' => 'User Mgmt',
    'editUser' => 'User Mgmt'
];

$t = basename($_SERVER['REQUEST_URI']);
$t = (in_array($t, $titles)) ? $titles[$t] : 'Undefined'; 

echo '
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
';

echo "<title>{$t} | QA SERVER</title>";
include_once(dirname(__FILE__, 2).'/css.html');