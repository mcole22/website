<?php
require_once('../autoloader.php');

$user = new User(true);

if($user->rejectUser($_POST['uid'])) {
    echo "<script>alert('User account has been removed!'); window.close();</script>";
} else {
    echo "<script>alert('There was a critical error!'); window.close();</script>";
}