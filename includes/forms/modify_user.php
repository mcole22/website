<?php
require_once('../autoloader.php');

$user = new User(true);

foreach($_POST as $k => $v) {
    if (preg_match('/^[a-zA-Z0-9.@-]+$/', $v) == 0 && !empty($v))
        exit("Invalid {$k}!");

    $data[$k] = $v;
}

$r = $user->getField('uid, created_at, username, email, firstname, lastname, rid, approved, valid_from, valid_to', 'uid', $data['uid']);
$diff = array_diff_assoc($data, $r[0]);

if(!empty($diff)) {
    $user->setField($diff, $_POST['uid']);
    echo "<script>alert('Changes successfully saved!'); window.close();</script>";
} else {
    echo "<script>alert('No changes were made.'); window.close();</script>";
}