$('form.ajax').on('submit', function() {
    var form = $(this),
        action = form.attr('action'),
        method = form.attr('method'),
        formid = form.attr('id'),
        data = {};
  
    form.find('[name]').each(function(index, value) {
      var form = $(this),
          name = form.attr('name'),
          value = form.val();

      data[name] = value;
    });

    $.ajax({
        url: action,
        type: method,
        data: data,
        success: function(success) {
          alert(success);
        },
        error: function() {
          alert("Critical error!");
        }
    });
  
    return false;
  });